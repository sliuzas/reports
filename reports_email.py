import base64
import smtplib

from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

from apiclient import errors
import httplib2
from apiclient import discovery

import charts
import pandas as pd
import numpy as np
import credentials


def send_message(service, user_id, message):
    """Send an email message.

    Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

    Returns:
    Sent Message.
    """
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)

def get_emails():
    return pd.read_csv('emailsteste.csv')


def create_message(strFrom,strTo):

    # Create the root message and fill in the from, to, and subject headers
    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = 'Relatório de desempenho GoBots'
    msgRoot['From'] = strFrom
    msgRoot['To'] = strTo

    # Encapsulate the plain and HTML versions of the message body in an
    # 'alternative' part, so message agents can decide which they want to display.

    # We reference the image in the IMG SRC attribute by the ID we give it below
    msgText = MIMEText('<h1><center><b>Relatório de Desempenho Semanal GoCommerce </b></center></h1>'
                       '<p><center><font size="4">Olá, segue abaixo um resumo do desempenho do GoCommerce dessa semana.</font>'
                       '<p><center><font size="4">Para mais informações sobre o desempenho clique '
                       '<a href="https://gocommerce.gobots.com.br/painel">aqui</a>'
                       ' ou na imagem abaixo.</font><center></p>'
                       '<a href="https://gocommerce.gobots.com.br/painel">'
                       ' <img src="cid:image1" style="display: block;margin-left: auto;margin-right: auto;"></a><br>'
                       '<a href="https://gocommerce.gobots.com.br/painel">'
                       ' <img src="cid:image2" style="display: block;float: center;width: 39%;"></a><br>',
                       'html')

    # This example assumes the image is in the current directory
    fp = open('email.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    fp = open('gocommercegreen.png', 'rb')
    msgImage2 = MIMEImage(fp.read())
    fp.close()

    # Define the image's ID as referenced above
    msgImage.add_header('Content-ID', '<image1>')
    msgImage2.add_header('Content-ID', '<image2>')
    msgRoot.attach(msgText)
    msgRoot.attach(msgImage)
    msgRoot.attach(msgImage2)


    # Send the email (this example assumes SMTP authentication is required)
    # Send the message via local SMTP server.
    return {'raw': base64.urlsafe_b64encode(msgRoot.as_string().encode('UTF-8')).decode('ascii')}


if __name__ == "__main__":
    list_emails = get_emails()
    credentials = credentials.get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    for index, row in list_emails.iterrows():
        charts.set_charts(row['email'])
        send_message(service, "victor@gobots.com.br", create_message('victor@gobots.com.br', 'pedrosliuzas@gmail.com'))
        if not pd.isnull(row['email2']):
            send_message(service, "victor@gobots.com.br", create_message('victor@gobots.com.br', 'pedrosliuzas@gmail.com'))
        if not pd.isnull(row['email3']):
            send_message(service, "victor@gobots.com.br", create_message('victor@gobots.com.br', 'pedrosliuzas@gmail.com'))
