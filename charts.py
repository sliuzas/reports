import urllib.request

import plotly
import plotly.graph_objs as go
import json
import plotly.tools as tls
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageChops
from selenium import webdriver

token = {"Authorization":"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwZXJndW50ZWFxdWkiLCJyb2xlcyI6WyJST0xFX0FETUlOIiwiUk9MRV9NQVNURVIiXX0.YkTo2tiu9sEK7QrVRRZCcsnW0PKyw80kXVzghvYUGjQw4rhmckl4F6SwZKM6j87x_Gf6VVVkJj8EMNvOc1EPpA"}


def get_info(email):
    teste = urllib.request.Request(headers=token, url="https://askhere.gobots.com.br/merchants/"+email+"/analytics/charts/all?month=3&year=2018")
    t = urllib.request.urlopen(teste)
    return t.read()


def plot_line_pre_chart(attendant, bot, date, total):
    trace0 = go.Scatter(
        x=date,
        y=attendant,
        text=attendant,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#F5C941'
        ),
        mode='lines+markers+text',
        name='Equipe de Atendimento',
        marker=dict(
            color='#F5C941',
        )
    )
    trace1 = go.Scatter(
        x=date,
        y=bot,
        legendgroup='pre',
        text=bot,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#1E6FB8'
        ),
        mode='lines+markers+text',
        name='GoCommerce - Pré',
        marker=dict(
            color='#1E6FB8',
        )
    )

    trace2 = go.Scatter(
        x=date,
        y=total,
        legendgroup='pre',
        text=total,
        textposition='bottom',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#43A046'
        ),
        mode='lines+markers+text',
        name='Total - Pré',
        marker=dict(
            color='#43A046',
        )
    )

    trace3 = go.Scatter(
        x=date,
        y=total-bot-attendant,
        legendgroup='pre',
        text=total-bot-attendant,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#575756'
        ),
        mode='lines+markers+text',
        name='Sem Resposta - Pré',
        marker=dict(
            color='#575756',
        )
    )
    return trace0, trace1, trace2, trace3


def plot_line_pos_chart(attendant, bot, automatic, costumer, sales, date, total):
    trace0 = go.Scatter(
        x=date,
        y=attendant,
        legendgroup='pos',
        text=attendant,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#F5C941'
        ),
        mode='lines+markers+text',
        name='Equipe de Atendimento - Pós',
        marker=dict(
            color='#F5C941',
        )
    )
    trace1 = go.Scatter(
        x=date,
        y=bot,
        legendgroup='pos',
        text=bot,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#1E6FB8'
        ),
        mode='lines+markers+text',
        name='GoCommerce - Pós',
        marker=dict(
            color='#1E6FB8',
        )
    )

    trace2 = go.Scatter(
        x=date,
        y=total,
        legendgroup='pos',
        text=total,
        textposition='bottom',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#43A046'
        ),
        mode='lines+markers+text',
        name='Total - Pós',
        marker=dict(
            color='#43A046',
        )
    )

    trace3 = go.Scatter(
        x=date,
        y=automatic,
        legendgroup='pos',
        text=automatic,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#B7B7B7'
        ),
        mode='lines+markers+text',
        name='Mensagens Automáticas - Pós',
        marker=dict(
            color='#B7B7B7',
        )
    )

    trace4 = go.Scatter(
        x=date,
        y=costumer,
        legendgroup='pos',
        text=costumer,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#575756'
        ),
        mode='lines+markers+text',
        name='Mensagens do Consumidor - Pós',

        marker=dict(
            color='#575756',
        )
    )

    trace5 = go.Scatter(
        x=date,
        y=sales,
        legendgroup='pos',
        text=sales,
        textposition='top',
        textfont=dict(
            family='sans serif',
            size=12,
            color='#FF8481'
        ),
        mode='lines+markers+text',
        name='Número de Vendas - Pós',
        marker=dict(
            color='#FF8481',
        )

    )
    return trace0, trace1, trace2, trace3, trace4, trace5


def plot_bar_pre_chart(attendant,bot,date,total):
    trace1 = go.Bar(
        x=date,
        y=total - bot - attendant,
        name='Sem Resposta - Pré',
        marker=dict(
            color='#575756'
        )
    )

    trace2 = go.Bar(
        x=date,
        y=attendant,
        name='Equipe de Atendimento - Pré',
        marker=dict(
            color='#F5C941'
        )

    )
    trace3 = go.Bar(
        x=date,
        y=bot,
        name='GoCommerce - Pré',
        marker=dict(
            color='#1E6FB8'
        )
    )

    return trace1, trace2, trace3


def plot_all(pre_attendant, pre_bot, date, pre_total, pos_attendant, pos_bot, pos_automatic,
             pos_customer, pos_sales, pos_total):

    trace0, trace1, trace2, trace3 = plot_line_pre_chart(pre_attendant, pre_bot, date, pre_total)
    trace7, trace8, trace9, trace10, trace11, trace12 = plot_line_pos_chart(pos_attendant, pos_bot, pos_automatic, pos_customer, pos_sales, date, pos_total)

    fig = tls.make_subplots(rows=2, cols=1, subplot_titles=('Pré-venda', 'Pós-venda'))

    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 1, 1)
    fig.append_trace(trace3, 1, 1)
    fig.append_trace(trace7, 2, 1)
    fig.append_trace(trace8, 2, 1)
    fig.append_trace(trace9, 2, 1)
    fig.append_trace(trace10, 2, 1)
    fig.append_trace(trace11, 2, 1)
    fig.append_trace(trace12, 2, 1)
    fig['layout']['yaxis1'].update(title='Número de Perguntas')
    fig['layout']['yaxis2'].update(title='Número de Mensagens')
    fig['layout'].update(legend=dict(x=1.1, y=0.5))

    config = {'showLink': False,
              'displayModeBar': False}
    plotly.offline.plot(fig, config=config, auto_open=False,
                        image_width=900, image_height=600)
    driver = webdriver.PhantomJS(executable_path="phantomjs-2.1.1-linux-x86_64/bin/phantomjs")
    driver.set_window_size(900, 600)
    driver.get('temp-plot.html')
    driver.save_screenshot('my_plot.png')
    im = Image.open('my_plot.png')
    trim(im).save('email.png')


def trim(im):
   bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
   diff = ImageChops.difference(im, bg)
   diff = ImageChops.add(diff, diff, 2.0, -100)
   bbox = diff.getbbox()
   if bbox:
      return im.crop(bbox)


def set_charts(email):
    j = json.loads(get_info(email))
    pre_sale = j['questions_information']
    pos_sale = j['chat_information']
    pre_attendant = [d['attendant'] for d in pre_sale]
    pre_bot = [d['bot'] for d in pre_sale]
    pre_date = [d['date'] for d in pre_sale]
    pre_total = [d['total'] for d in pre_sale]
    pos_attendant = [d['attendant_messages'] for d in pos_sale]
    pos_bot = [d['bot_messages'] for d in pos_sale]
    pos_total = [d['messages'] for d in pos_sale]
    pos_automatic = [d['automatic_messages'] for d in pos_sale]
    pos_customer = [d['customer_messages'] for d in pos_sale]
    pos_sales = [d['sales'] for d in pos_sale]

    plot_all(np.asarray(pre_attendant)[-7:], np.asarray(pre_bot)[-7:], np.asarray(pre_date)[-7:], np.asarray(pre_total)[-7:],
              np.asarray(pos_attendant)[-7:], np.asarray(pos_bot)[-7:], np.asarray(pos_automatic)[-7:],
              np.asarray(pos_customer)[-7:], np.asarray(pos_sales)[-7:], np.asarray(pos_total)[-7:])
